# frozen_string_literal: true

# BEGIN
def anagramm_filter(source_word, words)
  count_chars = lambda do |word|
    word
      .chars
      .each_with_object(Hash.new(0)) { |c, acc| acc[c] += 1 }
  end

  source_counter = count_chars.call(source_word)

  words.filter { |word| count_chars.call(word) == source_counter }
end
# END
