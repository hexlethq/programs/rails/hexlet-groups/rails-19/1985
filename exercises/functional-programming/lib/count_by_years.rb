# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  men = users.filter { |user| user[:gender] == 'male' }
  years = men.map { |user| user[:birthday].split('-').first }
  years.each_with_object(Hash.new(0)) { |year, acc| acc[year] += 1 }
end
# END
