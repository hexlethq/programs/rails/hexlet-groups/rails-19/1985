# frozen_string_literal: true

# BEGIN
def get_same_parity(nums)
  return [] if nums.empty?

  first, *rest = nums
  first_is_even = first.even?
  [first] + rest.filter { |num| num.even? == first_is_even }
end
# END
