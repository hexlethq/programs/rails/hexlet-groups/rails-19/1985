# frozen_string_literal: true

# BEGIN
def reverse(str)
  str
    .chars
    .reduce([]) { |a, e| a.unshift(e) }
    .join
end
# END
