# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return '' if start > stop

  number_to_fizz_buzz = lambda do |i|
    if (i % 5).zero? && (i % 3).zero?
      'FizzBuzz'
    elsif (i % 5).zero?
      'Buzz'
    elsif (i % 3).zero?
      'Fizz'
    else
      i
    end
  end

  (start..stop)
    .map(&number_to_fizz_buzz)
    .join(' ')
end
# END
