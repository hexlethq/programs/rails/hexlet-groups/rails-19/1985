# frozen_string_literal: true

# BEGIN
def compare_versions(a, b)
  a_parts = a.split('.').map(&:to_i)
  b_parts = b.split('.').map(&:to_i)

  a_parts.zip(b_parts).each do |a_version, b_version|
    return 1 if a_version > b_version
    return -1 if b_version > a_version
  end

  return 0
end
# END
