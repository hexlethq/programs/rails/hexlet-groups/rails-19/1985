# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string(params)
  params
    .sort_by { |key, _| key }
    .map { |(key, value)| "#{key}=#{value}" }
    .join('&')
end
# END
# rubocop:enable Style/For
