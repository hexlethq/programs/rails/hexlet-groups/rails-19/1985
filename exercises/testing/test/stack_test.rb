# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def test_push_and_pop
    stack = Stack.new
    stack.push! :a
    assert stack.to_a == [:a]

    stack.push! :b
    assert stack.to_a == %i[a b]

    assert stack.pop! == :b
    assert stack.to_a == %i[a]

    stack.push! :c
    assert stack.to_a == %i[a c]

    assert stack.pop! == :c
    assert stack.to_a == %i[a]
  end

  def test_size_and_empty?
    stack = Stack.new
    assert stack.empty?
    assert stack.size.zero?

    stack.push! :a
    refute stack.empty?
    assert stack.size == 1
  end

  def test_clear!
    stack = Stack.new(%i[a b c])
    stack.clear!

    assert stack.empty?
  end


  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
